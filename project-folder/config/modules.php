<?php

/**
 * Register application modules
 */
$application->registerModules(array(
    'frontend' => array(
        'className' => 'Apps\Modules\Frontend\Module',
        'path' => __DIR__ . '/../Apps/Modules/Frontend/Module.php'
    ),
    'backend' => array(
        'className' => 'Apps\Modules\Backend\Module',
        'path' => __DIR__ . '/../Apps/Modules/Backend/Module.php'
    )
));
