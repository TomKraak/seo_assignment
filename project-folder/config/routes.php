<?php

    // Get the router:
    $router = $di->get("router");

    foreach ($application->getModules() as $key => $module) {
        // Get the namespace based upon the Module it's classname:
        $namespace = str_lreplace('Module','Controllers', $module["className"]);
        // Add routes:
        $router->add('/'.$key.'/:params', array(
            'namespace' => $namespace,
            'module' => $key,
            'controller' => 'index',
            'action' => 'index',
            'params' => 1
        ))->setName($key);
        $router->add('/'.$key.'/:controller/:params', array(
            'namespace' => $namespace,
            'module' => $key,
            'controller' => 1,
            'action' => 'index',
            'params' => 2
        ));
        $router->add('/'.$key.'/:controller/:action/:params', array(
            'namespace' => $namespace,
            'module' => $key,
            'controller' => 1,
            'action' => 2,
            'params' => 3
        ));
    }

    $di->set("router", $router);


    /**
     * Function to replace the last occurance.
     * @param $search,          The needle to search for.
     * @param $replace,         The value to replace the last needle with.
     * @param $subject,         The text to search in.
     * @return String,          String with replaced values if occured.
     */
    function str_lreplace($search, $replace, $subject)
    {
        $pos = strrpos($subject, $search);

        if($pos !== false)
        {
            $subject = substr_replace($subject, $replace, $pos, strlen($search));
        }

        return $subject;
    }
?>
