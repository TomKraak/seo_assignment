<?php

namespace Apps\Modules\Backend;

use Phalcon\DiInterface;
use Phalcon\Loader;
use Phalcon\Mvc\View;
use Phalcon\Mvc\ModuleDefinitionInterface;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Flash\Session as FlashSession;

class Module implements ModuleDefinitionInterface
{
    /**
     * Registers an autoloader related to the module
     *
     * @param DiInterface $di
     */
    public function registerAutoloaders(DiInterface $di = null)
    {
    }

    /**
     * Registers services related to the module
     *
     * @param DiInterface $di
     */
    public function registerServices(DiInterface $di)
    {
        /**
         * Read the configuration
         */
        $config = require APP_PATH . "/Apps/Modules/Backend/config/config.php";

        /**
         * Setting up the view component
         */
        $view = $di['view'];
        $view->setViewsDir($config->application->viewsDir);
        $di['view'] = $view;


        /**
         * Register the session flash service with the Twitter Bootstrap classes
         */
        $di->set('flashSession', function () {
            return new FlashSession(
                array(
                    'error'   => 'alert alert-danger',
                    'success' => 'alert alert-success',
                    'notice'  => 'alert alert-info',
                    'warning' => 'alert alert-warning',
                    'site'    => 'errors'
                )
            );
        });

        /**
         * The URL component is used to generate all kinds of URLs in the application
         */
        $di->setShared('url', function () use ($config) {
            $url = new UrlResolver();
            $url->setBaseUri($config->application->baseUri);
            return $url;
        });

    }
}
