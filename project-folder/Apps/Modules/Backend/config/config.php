<?php

return new \Phalcon\Config(array(
    'application' => array(
        'migrationsDir'  => __DIR__ . '/../migrations/',
        'viewsDir'       => dirname(dirname(__FILE__)) . '/views/',
        'baseUri'        => '/'
    )
));
