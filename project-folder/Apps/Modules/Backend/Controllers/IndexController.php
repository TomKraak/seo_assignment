<?php

namespace Apps\Modules\Backend\Controllers;

class IndexController extends ControllerBase
{
    public function indexAction()
    {
    	// This will display the view located in Apps/Modules/Backend/views/index/index.phtml

    	// Get the assets collection:
    	$jsFooter = $this->assets->collection('footer');
    	// Add a script
    	$jsFooter->addJs('../public/js/test.js');
        // Add another script:
        $jsFooter->addJs('../public/js/example.js');
    }

}

