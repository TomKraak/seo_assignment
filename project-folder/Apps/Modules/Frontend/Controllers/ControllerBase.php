<?php

namespace Apps\Modules\Frontend\Controllers;

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
	/** Property to serve as DI for JS */
	protected $jsDI;

	/**
	 * Function that is executed when a Controller is instantiated.
	 */
	public function initialize(){
		// Set the layout:
		$this->view->setLayout('main');
		// Initialize the JS footer collection:
		$this->assets->collection('footer');
		// Initialize the JS DI:
		$this->jsDI = new \Apps\Source\Plugins\Js\Di($this->assets);
		$this->jsDI->add('init', '');
		// Enable the view:
		$this->view->enable();
	}

    /**
     * Function to re-direct where the user came from.
     */
    protected function redirectBack() {
    	// Re-direct to the previous url:
	    return $this->response->redirect($this->request->getHTTPReferer());
	}
}
