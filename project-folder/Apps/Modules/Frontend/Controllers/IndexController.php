<?php

namespace Apps\Modules\Frontend\Controllers;

class IndexController extends ControllerBase
{
    public function indexAction()
    {
    	// This will display the view located in Apps/Modules/Frontend/views/index/index.phtml
    }

    public function testAction(){
    	// The text action which will display the view located in Apps/Modules/Frontend/views/index/test.phtml
    }
}

