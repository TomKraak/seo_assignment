<?php

namespace Apps\Modules\Frontend\Controllers;

class TestController extends ControllerBase
{
    public function exampleAction()
    {
    	// This will display the view located in Apps/Modules/Frontend/views/test/example.phtml
    }

    public function otherExampleAction(){
    	// The text action which will display the view located in Apps/Modules/Frontend/views/test/otherexample.phtml
    }
}

