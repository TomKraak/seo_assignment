<?php

namespace Apps\Modules\Frontend\Controllers;

class SomeOtherController extends ControllerBase
{

    public function methodAction(){
    	// This will display the view located in Apps/Modules/Frontend/views/some-other/method.phtml

    	// Get the assets collection:
    	$footer = $this->assets->collection('footer');
    	// Add a script
    	$footer->addJs('/public/js/test.js');
    	// Set the target path:
		$footer->setTargetPath('/public/js/final.js');
		// Join all added scripts
		$footer->join(true);
		 // Use the built-in Jsmin filter
	    $footer->addFilter(
	        new \Phalcon\Assets\Filters\Jsmin()
	    );

    }

}

