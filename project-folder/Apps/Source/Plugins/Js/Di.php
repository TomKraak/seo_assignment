<?php

	namespace Apps\Source\Plugins\Js;

	/****************************************************
	 *													*
	 * This class is used to serve as DI for JS			*
	 *													*
	 ****************************************************/
	class Di{

	 	/** The data */
	 	private $data;
	 	/** The assets */
	 	private $assets;
	 	/** The name of the DI var in JS */
	 	Const NAME = "DI";

	 	/**
	 	 * Constructor of this class.
	 	 * @param $assets,		Reference to the assets.
	 	 * @param $data,		Map of data. Default is empty array.
	 	 */
	 	public function __construct(&$assets, $data = array()){
	 		// Assign the assets:
	 		$this->assets = $assets;
	 		// Assign the data:
	 		$this->data  = $data;
	 	}

	 	/**
		 * Function to add a value to the Javascript Application object.
		 * @param $key,			The key to add it to.
		 * @param $value,		The value to add.
		 */
		public function add($key, $value){
			// Assign the value:
			$this->data[$key] = $value;
			// Set the assets collection to it:
			$this->assets->collection('footer')->addInlineJs(self::NAME . " = " . json_encode($this->data) . ";");
		}

	}
?>