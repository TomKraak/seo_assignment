<?php
    
    namespace Apps\Source\Components;

    /****************************************************************
     *                                                              *
     * Extendable singleton class.                                  *
     *                                                              *
     ****************************************************************/
    class Singleton {
        // The instances of the singletons.
        protected static $instances;
     
        // Private clone function to prevent cloning.
        private function __clone() { } 
        // Private construct function to prevent construction.
        private function __construct() { } 
        // Private wake function to prevent waking.
        private function __wakeup() { }
     
        /**
         * Function to get an instance of the class.
         * @return self,        Instance of this class.
         */
        final public static function getInstance() {
            // Get the called class:
            $class = get_called_class();
            // Create the instance of the called class if it does not exists yet:
            if (!isset(static::$instances[$class])) {
                static::$instances[$class] = new static();
            }
            return static::$instances[$class];
        }
    }
?>