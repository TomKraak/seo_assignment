<?php
	namespace Apps\Source\Components;

	/********************************************************************
     * 																	*
     * Class that is used to determine order types in a query 	 		*
     *																	*
     ********************************************************************/
	class OrderType
	{
		/** The ascending type */
		Const ASC = "ASC";
		/** The descending type */
		Const DESC = "DESC";
	}

?>