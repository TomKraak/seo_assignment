<?php

	namespace Apps\Source\Components;

	use Apps\Source\Components\OrderType as OrderType;
	use Phalcon\Paginator\Adapter\Model as Paginator;

	/********************************************************************
     * 																	*
     * Class that is used to serve as a base for each model class 		*
     *																	*
     ********************************************************************/
	class BaseModel extends \Phalcon\Mvc\Model
	{
		/** The default pagination limit */
		Const PAGINATION_LIMIT = 10;
		/** The paginator that is used */
		private static $paginator = null;

		/**
		 * Function to find one specific model based on the given parameters.
		 * @param $params,				Array of key value pairs to use in the find condition.
		 * @param $orderBy,				Array of key value pairs to order by, key as column, value as order type. @see OrderType.
		 * @return ResultSet			Phalcon result set.
		 */
		public static function findByAttributes($params = array(), $orderBy = array()){
			// Query it:
			$result = parent::findFirst(self::getQueryData($params, $orderBy));
			return $result === false ? null : $result;
		}

		/**
		 * Function to find all objects of this model based on the given parameters.
		 * @param $params,				Array of key value pairs to use in the find condition.
		 * @param $orderBy,				Array of key value pairs to order by, key as column, value as order type. @see OrderType.
		 * @param $page,				The page to use. Default no pagination.
		 * @param $pageLimit,			The limit to use for each page. Defaults to @see BaseModel::PAGINATION_LIMIT.
		 * @return ResultSet 			Phalcon result set.
		 */
		public static function findAllByAttributes($params = array(), $orderBy = array(), $page = null, $pageLimit = self::PAGINATION_LIMIT){
			// Query it:
			$models = parent::find(self::getQueryData($params, $orderBy));
			// Apply pagination if needed:
			self::applyPagination($models, $page, $pageLimit);
			// Return the models:
			return $models;	        
		}

		/**
		 * Function to get the paginator that was used.
		 * @return Paginator,			The used paginator for this model, null otherwise.
		 */
		public static function getPaginator(){
			return self::$paginator;
		}



		/*---------------------------------------------------------------------------
		 * Private methods
		 *--------------------------------------------------------------------------*/

		/**
		 * Function to apply pagination if needed.
		 * @param $models,				The models to apply the pagination for.
		 * @param $page,				The page to use. Null for none.
		 * @param $pageLimit,			The limit to use for each page.
		 */
		private static function applyPagination(&$models, $page, $pageLimit){
			// Check if we need to use pagination:
			if(!is_null($page) && is_numeric($page) && $page >= 0 && is_numeric($pageLimit) && $pageLimit >= 0){
				// Create the paginator:
				self::$paginator = new Paginator(array(
		            "data" => $models,
		            "limit"=> $pageLimit,
		            "page" => $page
		        ));
		        $paginateData = self::$paginator->getPaginate();
		        // Set the models to only contain the paginated items:
		        $models = $paginateData->items;
			}
		}

		/**
		 * Function to find one specific model based on the given parameters.
		 * @param $params,				Array of key value pairs to use in the find condition.
		 * @param $orderBy,				Array of key value pairs to order by, key as column, value as order type. @see OrderType.
		 * @return Assoc array,			Array with condition, bind and order for Phalcon.
		 */
		private static function getQueryData($params, $orderBy){
			// Get the find data:
			$findData = self::getFindData($params);
			// Add order by if needed:
			self::addOrderBy($findData, $orderBy);
			// Return the data:
			return $findData;
		}

		/**
		 * Function to get the data for the find query.
		 * @param $params,				Array of key value pairs to use in the find condition.
		 * @return Assoc array,			Array with condition and bind key for Phalcon.
		 */
		private static function getFindData($params){
			// Make sure we get the expected parameter:
			if(!is_array($params) || count($params) < 1){
				return array("");
			}
			// Define the conditions:
			$conditions = array();
			// Define the bind array:
			$bind = array();
			// Add conditions and bind values:
			foreach ($params as $key => $value) {
				// Add the condition:
				$conditions[] = $key . " = :" . $key . ":";
				// Add the binding:
				$bind[$key] = $value;
			}
			// Define the condition:
			$condition = implode(' AND ', $conditions);
			// Return the data:
			return array($condition, 'bind' => $bind);
		}

		/**
		 * Function to add the order by params if needed.
		 * @param $findData,			Data retrieved from the getFindData method which is in Phalcon format.
		 * @param $orderBy,				Array of key value pairs to order by, key as column, value as order type. @see OrderType.
		 */
		private static function addOrderBy(&$findData, $orderBy){
			// Make sure we get the expected parameter for order by:
			if(is_array($orderBy) && count($orderBy) > 0){
				$orderData = array();
				foreach ($orderBy as $key => $orderType) {
					// Make sure the order type is valid:
					if($orderType != OrderType::ASC && $orderType != OrderType::DESC){
						// Use ASC as default, just like SQL does:
						$orderType = OrderType::ASC;
					}
					// Add the order data:
					$orderData[] = $key . " " . $orderType;
				}
				// Define the order string:
				$orderByString = implode(', ', $orderData);
				// Add the order by:
				$findData['order'] = $orderByString;
			}
		}

		/**
		 * Function to find a random model.
		 * @param $filter, 				array of values to filter out based on the column.
		 * @param $params,				The params to find the maximum with.
		 */
		public static function getRandomFirst($filter = array(), $params = array('column' => 'id'), $condition = '>=', $value = null){
			if(is_null($value)){
				// Get the max id value:
				$maxId = parent::maximum($params);
				// Get random value between 1-max: (max excluded)
				$randomValue = rand(1, $maxId);
				// Assign the value:
				$value = $randomValue;
			}
			// Define default conditions:
			$conditions = $params['column'] . ' ' . $condition . ' :' . $params['column'] . ':';
			// Define default bind:
			$bind = array('id' => $value);

			// Adjust bind and condition if filter is given:
			if(count($filter) > 0){
				$conditions .= ' AND ' . $params['column'] . ' NOT IN ({values:array})';
				$bind['values'] = $filter;
			}
			// Find one:
			$result = parent::findFirst(array('conditions' => $conditions, 'bind' => $bind));
			// If not found re-run same but other condition:
			if(!$result && $condition != '<'){
				return self::getRandomFirst($filter, $params, '<', $value);
			}
			// Find first where the id is bigger or equal then the random value:
			return $result;
		}

		/**
		 * Function to get an amount of random models.
		 */
		public static function getRandom($amount, $filter = array(), $params = array('column' => 'id')){
			// Define the list of randomModels:
			$randomModels = array();
			// Try to find the requested amount of random models:
			for ($i = 0; $i < $amount; $i++) {
				// Get a random model:
				$model = self::getRandomFirst($filter, $params);
				// Check if a model is found:
				if($model){
					// Add the model:
					$randomModels[] = $model;
					// Add the column to the filter:
					$filter[] = $model->$params['column'];
					continue;
				}
				// No more models are being found:
				break;
			}
			// Return the result:
			return $randomModels;
		}

	}

?>