<?php
	
	namespace Apps\Source\Components;

	/************************************************************
	 *															*
	 * This class is used to help with generation of files  	*
	 *															*
	 ************************************************************/
	 class FileManager extends Singleton{
	 	/** The default rights */
	 	Const RIGHTS_DEFAULT = 0755;
	 	
	 	/**
	 	 * Function to create a directory with the given rights, if it does not exists.
	 	 * @param $path,			The path to the directory.
	 	 * @param $rights, 			The rights to use. Default is @see self::RIGHTS_DEFAULT.
	 	 */
	 	public function createDirectory($path, $rights = self::RIGHTS_DEFAULT){
	 		// Check if directory exists:
	 		if($this->exists($path)){
	 			return;
	 		}
	 		// Create directory:
	 		mkdir($path, $rights, true);
	 	}

	 	/**
	 	 * Function to check if a file or directory exists and is writeable.
	 	 * @param $path,			The path to check for.
	 	 * @return bool,			True if it exists, false otherwise.
	 	 */
	 	private function exists($path){
 			// Check if path exists and is writable and return that:
 			return (file_exists($path) && is_writable($path));
	 	}

	 	/**
	 	 * Function to delete a file if it exists.
	 	 * @param $path, 			The path to the file to delete.
	 	 * @return bool,			True if it is deleted, false otherwise.
	 	 */
	 	public function delete($path){
	 		// Check if file exists:
	 		if($this->exists($path)){
	 			// Return the deletion status:
	 			return unlink($path);
	 		}
	 		// Failed to delete:
	 		return false;
	 	}
	 
	 }

 ?>