<?php

namespace Apps\Source\Components;


class ResponseUtils
{

    //Web Status codes:
    Const STATUS_OK = 200;
    Const STATUS_BAD_REQUEST = 400;
    Const STATUS_UNAUTHORIZED = 401;
    Const STATUS_PAYMENT_REQURIED = 402;
    Const STATUS_FORBIDDEN = 403;
    Const STATUS_NOT_FOUND = 404;
    Const STATUS_CONFLICT = 409;
    Const STATUS_INTERNAL_ERROR = 500;
    Const STATUS_NOT_IMPLEMENTED = 501;

    Const STATUS_USER_BLOCKED = 1000;
    Const STATUS_WRONG_PARAMETERS = 300;



    /**
     * Function to send a response.
     * @param $status,              The status code to send. Default is @see ResponseUtils::STATUS_OK
     * @param $body,                Assoc array of data to send. Default is none.
     * @param $content_type,        The content type to serve. Default is 'application/json'.
     */
    public static function sendResponse($status = self::STATUS_OK, $body = array(), $content_type = 'application/json')
    {
        // Set the status
        $status_header = 'HTTP/1.1 ' . self::STATUS_OK . ' ' . self::getStatusCodeMessage($status);
        header($status_header);
        // and the content type
        header('Content-type: ' . $content_type);

        $result = array(
            'status' => $status,
            'message' => self::getStatusCodeMessage($status),
            'result' => $body,
        );

        echo json_encode($result);
        exit;
    }

    /**
     * Function to get the header for a status code.
     * @param $status, The status code.
     * @return string, A message header for a status code.
     */
    private static function getStatusCodeMessage($status)
    {
        $message = "";
        switch($status){
            case self::STATUS_OK:
                $message = "OK";
                break;
            case self::STATUS_BAD_REQUEST:
                $message = 'Bad Request';
                break;
            case self::STATUS_UNAUTHORIZED:
                $message = 'Unauthorized';
                break;
            case self::STATUS_PAYMENT_REQURIED:
                $message = 'Payment Required';
                break;
            case self::STATUS_FORBIDDEN:
                $message = 'Forbidden';
                break;
            case self::STATUS_NOT_FOUND:
                $message = 'Not Found';
                break;
            case self::STATUS_CONFLICT:
                $message = 'Conflict';
                break;
            case self::STATUS_INTERNAL_ERROR:
                $message = 'Internal Server Error';
                break;
            case self::STATUS_NOT_IMPLEMENTED:
                $message = 'Not Implemented';
                break;
            case self::STATUS_USER_BLOCKED:
                $message = 'User is blocked';
                break;
            case self::STATUS_WRONG_PARAMETERS:
                $message = 'Wrong parameters are given';
                break;
            default:
                $message = "Unknown";
                break;
        }
        return $message;
    }

}
