<div class="container">
	<h2>This is the view for the index action of the IndexController in the Frontend module.</h2>
	<p>Click <?= $this->tag->linkTo(['manager/index', 'here']) ?> to go to the Backend module.</p>
</div>