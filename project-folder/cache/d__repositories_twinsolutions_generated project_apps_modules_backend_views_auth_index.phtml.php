<div class="container-fluid login-full-width">
	<!-- Error messages: -->
	<?= $this->flashSession->output() ?>
	<div id="login-container">
		<div id="login-header">
			<h2>Login</h2>
		</div>
		<div id="login-content">
			<!-- Start of the form: -->
			<?= $this->tag->form(['auth/login', 'method' => 'post', 'id' => 'login-form']) ?>
			    <!-- The username: -->
			  	<?= $form->render('username') ?>
			  	<!-- The password: -->
			  	<?= $form->render('password') ?>
			  	<!-- The csrf -->
			  	<?= $this->tag->hiddenField([$this->security->getTokenKey(), 'value' => $this->security->getToken()]) ?>
			  	<!-- The submit button: -->
			  	<?= $form->render('submit') ?>
			<!-- End of the form -->
			<?= $this->tag->endForm() ?>
		</div>
	</div>
</div>