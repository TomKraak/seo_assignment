<body>
	<!-- Content -->
	<?= $this->getContent() ?>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Latest compiled and minified bootstrap -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <!-- CSS stylesheet for the CMS -->
    <link rel="stylesheet" type="text/css" href="/public/css/cms.css">
    
    <!-- JS -->
    <?= $this->assets->outputInlineJs('footer') ?>
    <?= $this->assets->outputJs('footer') ?>

</body>
