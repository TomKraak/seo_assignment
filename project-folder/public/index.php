<?php

use Phalcon\Mvc\Application;

define('APP_PATH', realpath('..'));

try {

    /**
     * Read the configuration
     */
    $config = require APP_PATH . "/config/config.php";

    /**
     * Include services
     */
    require APP_PATH . '/config/services.php';

    /**
     * Handle the request
     */
    $application = new Application($di);

    /**
     * Include modules
     */
    require APP_PATH . '/config/modules.php';

    /**
     * Include routes
     */
    require __DIR__ . '/../config/routes.php';

    // Handle the application:
    echo $application->handle()->getContent();
} 
catch (\Exception $e) {
    echo $e->getMessage() . '<br>';
    echo '<pre>' . $e->getTraceAsString() . '</pre>';
}
