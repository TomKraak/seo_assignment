
exampleAlert();

function outputLog(){
	console.log('test');
}

/**
 * Some other method wiht a comment
 * @param data,			The data to output in a alert.
 */
 function outputAlert(data){
 	window.alert(data);
 }

 // Function that displays an example alert
 function exampleAlert(){
 	// Make the alert:
 	window.alert('This is an example alert!');
 }