<?php
	
	namespace Apps\Source\Plugins\Minification;

	use Phalcon;

	/****************************************************************
	 *																*
	 * Class to help with Minification 								*
	 *																*
	 ****************************************************************/
	 class AssetsManager extends Phalcon\Assets\Manager{

	 	/** List of collections */
	 	private $collections;

	 	/**
	 	 * Function to output the collection.
	 	 * @param $collection,			The collection to output.
	 	 * @param $callback,			The callback,
	 	 * @param $type,				The type of resource (css/js)
	 	 */
	 	public function output(Phalcon\Assets\Collection $collection, $callback, $type){
	 		// Check if we have the file output already:
	 		if(!$this->needsRegeneration($collection)){
	 			// The file already exists, and does not need to be generated again, output the file based upon type:
	 			switch($type){
	 				case 'css':
	 					echo \Phalcon\Tag::stylesheetLink( $collection->getTargetUri());
	 					break;
					case 'js':
						echo \Phalcon\Tag::javascriptInclude( $collection->getTargetUri());
						break;
	 			}
	 			return;
	 		}
	 		// File does not exist, has to be created, call parent (original) outputting method:
 			parent::output($collection, $callback, $type);		
	 	}

	 	/**
	 	 * Function to get/set a collection
	 	 * @param $name,			The name of the collection.
	 	 * @return Collection,		Collection object.
	 	 */
	 	public function collection($name){
 			// Check if collections are defined:
	 		if(!$this->collections){
	 			// Define collections as a list:
	 			$this->collections = array();
	 		}
	 		// Check if the collection does not exists yet:
	 		if(!isset($this->collections[$name])){
	 			// Create the collection:
	 			$this->collections[$name] = new Collection();
	 			// Use the parent setter for collection:
	 			parent::set($name, $this->collections[$name]);
	 		}
	 		// Return the collection by it's name:
	 		return $this->collections[$name];
	 	}

	 	/**
	 	 * Function to check if a collection needs to be regenerated.
	 	 * @param $collection,		The collection to check for.
	 	 * @return bool,			True if it needs to be regenerated, false otherwise.
	 	 */
	 	private function needsRegeneration($collection){
	 		// TODO: loop over the resources in the collection,
	 		// TODO: Check their file modification time by using filemtime(path)
	 		// TODO: Define a name based upon all file modifications AND the targetPath of this collection.
	 		// TODO: Check if file exists, if so, no need for updating, otherwise it needs updating.

	 		// For now, always regenarate:
	 		return true;
	 	}
	 }

 ?>