<?php
	
	namespace Apps\Source\Plugins\Minification;

	use Phalcon;

	/****************************************************************
	 *																*
	 * Class to help with Minification 								*
	 *																*
	 ****************************************************************/
	 class Collection extends Phalcon\Assets\Collection{

	 	/** Collection types */
	 	Const TYPE_JS = 0;
	 	Const TYPE_CSS = 1;
	 	Const TYPE_UNDEFINED = 2;

	 	/** The type of collection */
	 	private $collectionType = self::TYPE_UNDEFINED;

	 	/**
	 	 * Function to add CSS to the assets manager.
	 	 * @param $path,				The path to the file.
	 	 * @param $local,				Boolean, whether it is a local file or not. Defaults to true.
	 	 * @param $filter,				Boolean, Whether to apply the given filters. Defaults to true.
	 	 * @param $attributes,			Attributes, Defaults to empty list.
	 	 */
	 	public function addCss($path, $local = true, $filter = true, $attributes = array()){
	 		// Handle type:
	 		$this->handleType(self::TYPE_CSS);
	 		// Handle root links:
	 		$this->handleBaseAddition($path, $local);
	 		// Use parent method:
	 		return parent::addCss($path, $local, $filter, $attributes);
	 	}

	 	/**
	 	 * Function to add JS to the assets manager.
	 	 * @param $path,				The path to the file.
	 	 * @param $local,				Boolean, whether it is a local script or not. Defaults to true.
	 	 * @param $filter,				Boolean, Whether to apply the given filters. Defaults to true.
	 	 * @param $attributes,			Attributes, Defaults to empty list.
	 	 */
	 	public function addJs($path, $local = true, $filter = true, $attributes = array()){
	 		// Handle type:
	 		$this->handleType(self::TYPE_JS);
	 		// Handle root links:
	 		$this->handleBaseAddition($path, $local);
	 		// Use parent method:
	 		return parent::addJs($path, $local, $filter, $attributes);
	 	}

	 	/**
	 	 * Function to set the target path.
	 	 * @param $targetPath,			The path to the file.
	 	 */
	 	public function setTargetPath($targetPath){
	 		// Get the original target:
	 		$target = $targetPath;
	 		// Handle root links:
	 		$base = $this->handleBaseAddition($targetPath);
	 		// Use parent method:
	 		parent::setTargetPath($targetPath);
	 		// If base URI by / symbol was used:
	 		if($base){
				// Set the target URI (value that the script tag is using)
				parent::setTargetUri($target);
	 		}
	 	}

	 	/**
	 	 * Function to minify the file
	 	 */
	 	public function minify(){
	 		switch($this->collectionType){
	 			case self::TYPE_UNDEFINED:
	 				error_log("Unknown type to use for minification. InlineCSS? InlineJS or other unhandled type maybe?");
	 				break;
 				case self::TYPE_JS:
 					// Use the built-in Jsmin filter for javascript type:
				    $this->addFilter(new \Phalcon\Assets\Filters\Jsmin());
				    break;
			    case self::TYPE_CSS:
			    	// Use the built-in Cssmin filter for CSS type:
			    	$this->addFilter(new \Phalcon\Assets\Filters\Cssmin());
			    	break;
	 		}
	 	}

	 	/**
	 	 * Function to handle the complete path addition if it started with a slash to use the ROOT folder.
	 	 * @param $path,				The path tot he file as a reference.
 	 	 * @param $local,				Whether it is a local script or not.
 	 	 * @return bool, 				True if base was added, false otherwise.
	 	 */
	 	private function handleBaseAddition(&$path, $local = null){
	 		// Use source base path if path starts with a slash and local is null:
	 		if((is_null($local) || $local === true) && count($path) > 0 && $path[0] == '/'){
	 			// Add the base:
	 			$path = APP_PATH . $path;
	 			return true;
	 		}
	 		return false;
	 	}

	 	/**
	 	 * Function to handle the type.
	 	 * @param $type,				The type to set.
	 	 */
	 	private function handleType($type){
	 		// Check if CSS is added to a JS collection or vice versa:
	 		if($this->collectionType != self::TYPE_UNDEFINED && $this->collectionType != $type){
	 			throw new \Exception(
	 				"The collection already has a different type, than the type you're adding! ".
	 				"Can only be a collection of either JS or CSS, not both!", 1
 				);
	 		}
	 		// Valid to set the type:
	 		$this->collectionType = $type;
	 	}
	 }

 ?>