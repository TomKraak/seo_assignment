<?php
	namespace Apps\Source\Plugins\Routing;

	// Make sure the APP_PATH is defined.
	if(!defined('APP_PATH')){
		echo "Fatal error: Router.php line 6. APP_PATH must be defined!"; die();
	}


	/****************************************************************
	 * 																*
	 * This class is used for helping with routing. 				*
	 *																*
	 ****************************************************************/
	 class Router extends \Phalcon\MVC\User\Plugin {
	 	
	 	/** 
	 	 * The modules this router is routing for 
	 	 */
	 	private $modules;

	 	/** 
	 	 * The name of the folder controllers are saved in 
	 	 */
	 	Const CONTROLLERS_DIRECTORY_NAME = "Controllers";

	 	/**
	     * @var \Phalcon\Mvc\Router
	     */
	    protected $router;

 	    /**
	     * @var \Phalcon\DI
	     */
	    protected $di;

	    /**
	     * @var boolean
	     */
	    private $useSlugExtension;

	 	/**
	 	 * Constructor of this class.
	 	 * @param $useSlugExtension,		Boolean to determine if the Slug extension should be used. By default it won't.
	 	 * @param $provider,				Slug\IProvider, the slug provider. If null is given, defaults to DbProvider.
	 	 */
	 	public function __construct($useSlugExtension = false, Slug\IProvider $slugProvider = null){
	 		// Assign the router:
	 		$this->router = new \Phalcon\MVC\Router(false);
	 		// Assign the di:
	 		$this->di = $this->getDi();
	 		// Set the modules to none, by default:
	 		$this->modules = array();
	 		// Add routing for each of the modules:
	 		$this->addDefaultRoutes();
	 		// Set the slug usage:
	 		$this->setSlugUsage($useSlugExtension, $slugProvider);
	 	}

	 	/**
	 	 * Magic method to call functions on the router instead of this class if method not in this class.
	 	 */
	 	public function __call($method, $args = array()){
	 		// Make sure the arguments is an array. If it is one value, add it to an array:
	 		$args = is_array($args) ? $args : [$args];
	 		// Call the function from the router:
	 		return call_user_func_array(array($this->router, $method), $args);
	 	}

	 	/**
	 	 * Function to set the modules to route for.
	 	 * @param $modules,				The modules from the Phalcon\Mvc\Application.
	 	 */
	 	public function registerModules($modules){
	 		// Re-assign the di:
	 		$this->di = $this->getDi();
	 		// Assign the modules:
	 		$this->modules = $modules;
	 		// Add routing for the modules:
	 		$this->addDefaultRoutes();
	 	}

	 	/**
	 	 * Function to set the usage of the slug extension and execute it if needed.
	 	 * @param $useSlugExtension,		Boolean to determine if the Slug extension should be used.
	 	 * @param $slugProvider,			Slug\IProvider, the slug provider. If null is given, defaults to DbProvider.
	 	 */
	 	 private function setSlugUsage($useSlugExtension, Slug\IProvider $slugProvider = null){
	 	 	// Set the class property:
	 	 	$this->useSlugExtension = $useSlugExtension;
	 	 	// Check if slug should be used.
	 	 	if($this->useSlugExtension === true){
	 	 		// Activate slug:
	 	 		$slugManager = new Slug\SlugManager($slugProvider);
	 	 		$slugRoutes = $slugManager->getRoutes();
	 	 		// add routes:
	 	 		$this->addRoutes($slugRoutes);
	 	 	}
	 	 }	

	 	/**
	 	 * Function to add the routes for all the modules, controllers and actions as:
	 	 * module/controller/action
	 	 */
	 	private function addDefaultRoutes(){
	 		// Get the url map:
	 		$modules = $this->getModules();
	 		// Get the routes for the modules:
	 		$routes = $this->getRoutes($modules);
	 		// Add the routes:
	 		$this->addRoutes($routes);
	 	}

	 	/**
	 	 * Function to add a list of routes to the routing.
	 	 * @param $routes,			Route[]
	 	 */
	 	private function addRoutes($routes){
	 		// Loop trough the routes:
	 		foreach ($routes as $key => $route) {
	 			// echo 'route url: ' . $route->getUrl() . '<br>';
	 			// Check if the name of the route should be set:
	 			if($route->useName()){
	 				// Add the route with name:
					$this->router->add($route->getUrl(), $route->getRoute())->setName($route->getName());
	 				// Go to the next one:
	 				continue;
	 			}
	 			// Add the route without the name:
	 			$this->router->add($route->getUrl(), $route->getRoute());
	 		}
	 		// Set the router in the di:
	 		$this->di['router'] = $this->router;
	 		// Re-assign the di with the new router:
	 		$this->setDi($this->di);
	 	}

	 	/**
	 	 * Function to get the routes for the given map.
	 	 * @param $modules,			Objects\Module[] Array of modules.
	 	 * @return Route[],			Array of routes to add.
	 	 */
	 	private function getRoutes($modules){
	 		// Define a list of routes:
	 		$routes = array();
	 		// Add the routes from each module:
	 		foreach ($modules as $key => $module) {
	 			// Add the routes for this module:
	 			$routes = array_merge($routes, $module->getRoutes());
	 		}
	 		// Return the routes:
	 		return $routes;
	 	}


	 	/**
	 	 * Function to get the modules.
	 	 * @return Objects\Module[],			Array of modules
	 	 */
	 	private function getModules(){
	 		$modules = array();
	 		foreach ($this->modules as $name => $module) {
	 			// Add the created module
	 			$modules[] = Objects\Module::fromArray($module, $name);
	 		}
	 		// Return the modules:
	 		return $modules;
	 	}
	 }

?>