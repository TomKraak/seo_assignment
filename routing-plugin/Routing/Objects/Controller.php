<?php
	namespace Apps\Source\Plugins\Routing\Objects;

	/****************************************************************
	 * 																*
	 * This class is used to represent a Module.					*
	 *																*
	 ****************************************************************/
	 class Controller {

	 	/** The namespace of the controller */
	 	private $namespace;
	 	/** The actions of the controller */
	 	private $actions;
	 	/** The url of the controller */
	 	private $url;
	 	/** The name of the controller */
	 	private $name;
	 	/** The routes of the controller */
	 	private $routes;
	 	/** The url of the module of the controller */
	 	private $moduleUrl;
	 	/** Boolean if in default module. */
	 	private $defaultModule;

	 	/**
	 	 * Constructor of this class.
	 	 * @param $namespace,		The namespace of the module it's controllers.
	 	 * @param $name,			The name of the controller (i.e. IndexController)
	 	 * @param $moduleUrl,		The url of the module (i.e. manager)
	 	 */
	 	public function __construct($namespace, $name, $moduleUrl, $defaultModule){
	 		// Set the actions:
	 		$this->actions = array();
	 		// Set the namespace:
	 		$this->namespace = $namespace;
	 		// Set the name of the controller:
	 		$this->name = $name;
	 		// Set the url of the module:
	 		$this->moduleUrl = $moduleUrl;
	 		// Set the defaultModule value:
	 		$this->defaultModule = $defaultModule;
	 		// Load the data for this controller:
	 		$this->loadData();
	 	}

	 	/**
	 	 * Function to get the name
	 	 * @return String,		The name
	 	 */
	 	public function getName(){
	 		return $this->name;
	 	}

	 	/**
	 	 * Function to get the url
	 	 * @return String,		The url
	 	 */
	 	public function getUrl(){
	 		return $this->url;
	 	}

	 	/**
	 	 * Function to get the actions
	 	 * @return String[],		The actions
	 	 */
	 	public function getActions(){
	 		return $this->namespace;
	 	}

	 	/**
	 	 * Function to get the Routes of this controller.
	 	 * @return Objects\Route[],	Array of Route objects.
	 	 */
	 	public function getRoutes(){
	 		return $this->routes;
	 	}

	 	/**
	 	 * Load all the properties of this controller.
	 	 */
	 	private function loadData(){
	 		// Get the url:
	 		$this->url = $this->getControllerUrl($this->name);
	 		// Load the actions:
	 		$this->loadActions();
	 		// Load the routes:
	 		$this->loadRoutes();
	 	}

	 	/**
	 	 * Function to load the routes for this controller.
	 	 */
	 	private function loadRoutes(){
	 		// Define the routes:
	 		$this->routes = array();
			// Define url for default controller route:
			$url = ($this->defaultModule ? '' : ('/' . $this->moduleUrl)) . '/' . $this->url . '/:params';
			// Add default route:
			$this->routes[] = new Route($url, $this->namespace, $this->moduleUrl, $this->url, Route::DEFAULT_ACTION);

			// Loop trough the actions of the controller
			foreach ($this->actions as $key => $action) {
				// Define url for specific action route:
				$url = ($this->defaultModule ? '' : ('/' . $this->moduleUrl)) . '/' . $this->url . '/' . $this->getActionUrl($action) . '/:params';
				// Add specific route:
				$this->routes[] = new Route($url, $this->namespace, $this->moduleUrl, $this->url, $action);
			}
	 	}

	 	/**
	 	 * Function to load the actions from this controller.
	 	 */
	 	private function loadActions(){
	 		// Create a reflection class:
	 		$classToLoad = '\\' . $this->namespace . '\\' . $this->name;
	 		$reflectionClass = new \ReflectionClass($classToLoad);
	 		// Define the result:
	 		$this->actions = array();
	 		// Get the public methods:
	 		$methods = $reflectionClass->getMethods(\ReflectionMethod::IS_PUBLIC);
	 		// Add actions:
	 		foreach ($methods as $key => $method) {
	 			if(strpos($method->name, 'Action') !== false){
	 				// Get the name of the method:
	 				$methodName = str_replace('Action', '', $method->name);
	 				// Add the name of the action:
	 				$this->actions[] = $methodName;
	 			}
	 		}
	 	}

	 	/**
	 	 * Function to get the url representation of a controller.
	 	 * @param $controllerName,			The name of the controller.
	 	 * @return String,					A SEO friendly url.
	 	 */
	 	private function getControllerUrl($controllerName){
	 		// Get the pieces
	 		$pieces = preg_split('/(?=[A-Z])/', lcfirst($controllerName));
	 		// Glue the piecies together:
	 		$pieces = implode('-', $pieces);
	 		// Set all as lowercase:
	 		$pieces = strtolower($pieces);
	 		// Remove controller:
	 		$pieces = str_replace('-controller', '', $pieces);
	 		// Return the result:
	 		return $pieces;
	 	}

	 	/**
	 	 * Function to get the url representation fo a action.
	 	 * @param $actionName,				The name of the action.
	 	 * @return String,					A SEO friendly url.
	 	 */
	 	private function getActionUrl($actionName){
	 		// Get the pieces
	 		$pieces = preg_split('/(?=[A-Z])/', lcfirst($actionName));
	 		// Glue the piecies together:
	 		$pieces = implode('-', $pieces);
	 		// Set all as lowercase:
	 		$pieces = strtolower($pieces);
	 		// Return the result:
	 		return $pieces;
	 	}
	 }
 ?>