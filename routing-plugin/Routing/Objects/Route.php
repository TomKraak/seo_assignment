<?php
	namespace Apps\Source\Plugins\Routing\Objects;

	/****************************************************************
	 * 																*
	 * This class is used to represent a Route.						*
	 *																*
	 ****************************************************************/
	 class Route {

	 	/** The route array */
	 	private $route;
	 	/** The url */
	 	private $url;
	 	/** The name of this route */
	 	private $name;
	 	/** Whether to use a name */
	 	private $useName;
	 	/** The default controller */
	 	Const DEFAULT_CONTROLLER = "index";
	 	/** The default action */
	 	Const DEFAULT_ACTION = "index";
	 	/** The default params */
	 	Const DEFAULT_PARAM = 1;

	 	/**
	 	 * Constructor of this class.
	 	 * @param $url,				The url for the route.
	 	 * @param $namespace,		The namespace of the controller.
	 	 * @param $module,			The module name to route to.
	 	 * @param $controller,		The controller to route to. Defaults to index.
	 	 * @param $action,			The action to route to. Defaults to index.
	 	 * @param $useName,			Whether to use a name. Defaults to false.
	 	 * @param $param,			The param to use. Defaults to 1.
	 	 */
	 	public function __construct($url, $namespace, $module, $controller = self::DEFAULT_CONTROLLER, $action = self::DEFAULT_ACTION, $useName = false, $param = self::DEFAULT_PARAM){
	 		// Set the url:
	 		$this->url = strtolower($url);
	 		// Set the route:
	 		$this->route = array(
		        'namespace' => $namespace,
		        'module' => $module,
		        'controller' => $controller,
		        'action' => $action,
		        'params' => $param
			);
 			// Set the use name attribute:
 			$this->useName = $useName;
 			// Set the name:
 			$this->name = $this->useName === true ? $module : "";
	 	}

	 	/**
	 	 * Function to get the url
	 	 * @return String,		The url
	 	 */
	 	public function getUrl(){
	 		return $this->url;
	 	}

	 	/**
	 	 * Function to get the route
	 	 * @return Assoc array,			HashMap of attributes for the \Phalcon\MVC\Router.
	 	 */
	 	public function getRoute(){
	 		return $this->route;
	 	}

	 	/**
	 	 * Function to determine whether to use the name.
	 	 * @return boolean,				True if name should be used, false otherwise.
	 	 */
	 	public function useName(){
	 		return $this->useName;
	 	}

	 	/**
	 	 * Function to get the name of the route.
	 	 * @return String,				Name of the route.
	 	 */
	 	public function getName(){
	 		return $this->name;
	 	}
	 }
 ?>