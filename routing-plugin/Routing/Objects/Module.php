<?php
	namespace Apps\Source\Plugins\Routing\Objects;

	/****************************************************************
	 * 																*
	 * This class is used to represent a Module.					*
	 *																*
	 ****************************************************************/
	 class Module {

	 	/** The namespace of the module */
	 	private $namespace;
	 	/** The controllers of the module */
	 	private $controllers;
	 	/** The name of the module */
	 	private $name;
	 	/** The routes for the module */
	 	private $routes;
	 	/** The indicator if it is the default module */
	 	private $isDefault;
	 	/** The BaseController its name */
	 	Const CONTROLLER_BASE_NAME = "ControllerBase.php";


	 	/**
	 	 * Constructor of this class.
	 	 * @param $namespace,		The namespace of the module it's controllers.
	 	 * @param $controllers,		Controller[]. The controllers of the module.
	 	 * @param $name,			The name of the module.
	 	 * @param $default,			Bool to determine if this is the default module.
	 	 */
	 	public function __construct($namespace, $controllers, $name, $default){
	 		// Set the namespace:
	 		$this->namespace = $namespace;
	 		// Set the controllers:
	 		$this->controllers = $controllers;
	 		// Set the name:
	 		$this->name = $name;
	 		// Set the default value:
	 		$this->isDefault = $default;
	 		// Load the routes:
	 		$this->loadRoutes();
	 	}

	 	/**
	 	 * Function to load the routes for this module.
	 	 */
	 	private function loadRoutes(){
	 		// Define the routes:
	 		$this->routes = array();
	 		// Define url for default module route:
			$url = '/' . $this->name . '/:params';
			// Ãdd default route for this module:
			$this->routes[] = new Route(
				$url, $this->namespace, $this->name, Route::DEFAULT_CONTROLLER, Route::DEFAULT_ACTION, true
			);
			// Check if this is the default module:
			if($this->isDefault){
				// Also add the default module route without module name:
				$url = '/:params';
				$this->routes[] = new Route(
					$url, $this->namespace, $this->name, Route::DEFAULT_CONTROLLER, Route::DEFAULT_ACTION, true
				);
			}
			// Add the routes from all the controllers in this module:
			foreach ($this->controllers as $key => $controller) {
				$this->routes = array_merge($this->routes, $controller->getRoutes());
			}
	 	}

	 	/**
	 	 * Function to get the routes for this module.
	 	 * @return Objects\Route[],	Array of Route objects.
	 	 */
	 	public function getRoutes(){
	 		return $this->routes;
	 	}

	 	/**
	 	 * Function to get the url of the module.
	 	 * @return String,			The url of module.
	 	 */
	 	public function getUrl(){
	 		return $this->url;
	 	}

	 	/**
	 	 * Function to get the url
	 	 * @return String,		The url
	 	 */
	 	public function getNamespace(){
	 		return $this->namespace;
	 	}

	 	/**
	 	 * Function to get the route
	 	 * @return Objects\Controller[],		Array of controllers.
	 	 */
	 	public function getControllers(){
	 		return $this->controllers;
	 	}

	 	/**
	 	 * Function to create the Object\Module from an assoc array.
	 	 * @param $module, 						Assoc array from the modules.php in config folder.
	 	 * @param $url,							The url of the module (i.e. manager).
	 	 * @return Object\Module 				The module.
	 	 */	
	 	public static function fromArray($module, $url){
	 		// Define the controller namespace for this module:
 			$namespace = strrev(preg_replace(strrev("/Module/"),strrev('Controllers'),strrev($module['className']),1));
 			// Define the module controllers directory:
 			$controllersDirectory = APP_PATH . DIRECTORY_SEPARATOR . (str_replace('\\', DIRECTORY_SEPARATOR, $namespace));
 			// Define the default value:
 			$default = (isset($module['default']) && $module['default'] === true);
 			// Load the controllers for this module:
 			$controllers = self::loadControllers($namespace, $controllersDirectory, $url, $default);
 			// Create and return a module:
 			return new self($namespace, $controllers, $url, $default);
	 	}

	 	/**
	 	 * Function to load the controllers from a directory
	 	 * @param $namespace,				The namespace.
	 	 * @param $directory,				The path to the controllers directory.
 	 	 * @param $url,						The url of the module (i.e. manager).
 	 	 * @param $default,					Boolean to determine the default module.
	 	 * @return Controller[],			Array of Controllers for this module.
	 	 */
	 	private static function loadControllers($namespace, $directory, $url, $default){
	 		// Define the list of controllers:
			$controllers = [];
	 		// Check if the directory exists:
 			if(file_exists($directory)){
 				// Read the files from the directory:
 				$files = scandir($directory);
 				// Loop trough the files and make sure it is a file and the name contains Controller.
 				foreach ($files as $key => $file) {
 					if(is_file($directory . '/' . $file) && (strpos($file, 'Controller') !== false)){
 						// Add the controller if it's not a Base Controller:
 						if($file != self::CONTROLLER_BASE_NAME){
 							// Get the actual name of the controller, withotu extension:
 							$name = explode('.', $file)[0];
 							// Add the controller as a regular controller (non default module)
							$controllers[] = new Controller($namespace, $name, $url, false);
							// If it is the default module, also add the Controller without url prefix:
							if($default){
								// Add it without url prefix of the module:
								$controllers[] = new Controller($namespace, $name, $url, $default);
							}
 						}
 					}
 				}
 			}
 			// Return the result:
 			return $controllers;
	 	}
	 }
 ?>