<?php

	namespace Apps\Source\Plugins\Routing\Slug;

	/************************************************************************
	 *																		*
	 * Interface for providing Slug objects									*
	 *																		*
	 ************************************************************************/
	 interface IProvider{

	 	/**
	 	 * Function to provide a list of Slug objects as Array.
	 	 * @return Array,			List of Slug objects as associative array.
	 	 */
	 	 public function provide();

	 }