<?php

	namespace Apps\Source\Plugins\Routing\Slug;

	/************************************************************************
	 *																		*
	 * Provider for providing Slug objects from the database				*
	 *																		*
	 ************************************************************************/
	 class DbProvider implements IProvider{

	 	/**
	 	 * Function to provide a list of Slug objects as Array.
	 	 * @return Array,			List of Slug objects as associative array.
	 	 */
	 	 public function provide(){
	 	 	// Retrieve the slugs from the DB:
	 	 	$slugs = Slug::find();
	 	 	// Make sure slugs are found:
	 	 	if($slugs->count()){
	 	 		return $slugs->toArray();
	 	 	}
	 	 	// Return empty list:
	 	 	return array();
	 	 }

	 }