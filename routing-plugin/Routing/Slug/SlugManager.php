<?php
		
	namespace Apps\Source\Plugins\Routing\Slug;
	use Apps\Source\Plugins\Routing\Objects;

	/****************************************************************
	 * 																*
	 * This class is used for helping with custom routing by db.	*
	 *																*
	 ****************************************************************/
	 class SlugManager {

	 	/** The slug provider */
	 	private $provider;

	 	/**
	 	 * Constructor of this class.
	 	 * @param $provider,			IProvider, the slug provider. If null is given, defaults to DbProvider.
	 	 */
	 	public function __construct(IProvider $provider = null){
	 		// Check if provider is null:
	 		if(is_null($provider)){
	 			// Re-assign provider as DbProvider:
	 			$provider = new DbProvider();
	 		}
	 		// Assign the provider of this instance:
	 		$this->provider = $provider;
	 	}

	 	/**
	 	 * Function to get the routes for the slugs in the provider.
	 	 * @return Route[]			Array of Routes.
	 	 */
	 	public function getRoutes(){
	 		// Define the routes:
	 		$routes = array();
	 		// Load all the slugs from the provider:
	 		$slugs = $this->provider->provide();
	 		// Loop trough the slugs:
	 		foreach ($slugs as $key => $slug) {
	 			// Add the route:
	 			$routes[] = $this->getRoute($slug);
	 		}
	 		// Return the routes:
	 		return $routes;
	 	}

	 	/**
	 	 * Function to get a Route for the given slug.
	 	 * @param $slug,		The retrieved slug object from the provider.
	 	 * @return Route 		The route.
	 	 */
	 	private function getRoute($slug){
	 		// Use defaults if invalid controller and action:
	 		$slug['controller'] = is_null($slug['controller']) ? Route::DEFAULT_CONTROLLER : $slug['controller'];
	 		$slug['action'] = is_null($slug['action']) ? Route::DEFAULT_ACTION : $slug['action'];

	 		// Define the url:
	 		$url = !is_null($slug['params']) ? '/' . $slug['slug'] . '/:params' : '/' . $slug['slug'];
	 		$params = is_null($slug['params']) ? $slug['slug'] : $slug['params'];
	 		// Create and return the route object:
	 		return new Objects\Route($url, $slug['namespace'], $slug['module'], $slug['controller'], $slug['action'], false, $params);
	 	}

	 }

?>